using Godot;
using System;

public class Player : KinematicBody
{
    private int speed;
    [Export]
    private int walk_speed = 10;
    [Export]
    private int sprint_speed = 20;
    [Export]
    private int jump_speed = 5;
    [Export]
    private float acceleration = 4.5f;
    [Export]
    private float max_slope_angle = 80;
    [Export]
    private float mouse_sensitivity = 0.05f;
    [Export]
    public float gravity = 24.8f;
    [Export]
    public int push = 5;

    Area objectDetector;

    private Vector3 velocity = Vector3.Zero;
    private Vector3 direction = Vector3.Zero;

    private Camera camera;
    private Spatial head;

    private Pickup pickup;
    private ItemsManager itemsManager;

    public override void _Ready()
    {
        camera = GetNode<Camera>("Head/Camera");
        head = GetNode<Spatial>("Head");
        objectDetector = GetNode<Area>("ObjectDetector");
        
        pickup = GetNode<Pickup>("Modules/Pickup");
        itemsManager = GetNode<ItemsManager>("Head/ItemsManager");

        if(pickup != null)
        {
            pickup.init(camera);
            itemsManager.init(pickup);
        }

        Input.SetMouseMode(Input.MouseMode.Captured);
    }

    public override void _PhysicsProcess(float delta)
    {
        ProcessInput(delta);
        ProcessMovement(delta);
    }

    private void ProcessInput(float delta)
    {

        direction = new Vector3();
        Transform camXform = camera.GlobalTransform;

        if(Input.IsActionPressed("move_forward"))
            direction -= Transform.basis.z;
        if(Input.IsActionPressed("move_backward"))
            direction += Transform.basis.z;
        if(Input.IsActionPressed("move_left"))
            direction -= Transform.basis.x;
        if(Input.IsActionPressed("move_right"))
            direction += Transform.basis.x;

        direction = direction.Normalized();

        if(IsOnFloor())
        {
            if(Input.IsActionJustPressed("move_jump"))
                velocity.y = jump_speed;
        }

        if(Input.IsActionPressed("move_sprint") && IsOnFloor() && pickup.GetGrabbedObject() == null)
            speed = sprint_speed;
        else
            speed = walk_speed;

        if (Input.IsActionJustPressed("ui_cancel"))
        {
            if (Input.GetMouseMode() == Input.MouseMode.Visible)
                Input.SetMouseMode(Input.MouseMode.Captured);
            else
                Input.SetMouseMode(Input.MouseMode.Visible);
        }

        if(Input.IsActionPressed("Use") && itemsManager.GetCurrentItem() != null)
            itemsManager.GetCurrentItem().Use();
   
    }

    private void ProcessMovement(float delta)
    {

        direction.y = 0;

        velocity.y -= gravity * delta;

        Vector3 target = direction;

        target *= speed;

        velocity = velocity.LinearInterpolate(direction * speed, acceleration * delta);

        velocity = MoveAndSlide(velocity, new Vector3(0, 1, 0), false, 4, Mathf.Deg2Rad(max_slope_angle), false);

		var bodies = objectDetector.GetOverlappingBodies();

        foreach (PhysicsBody item in bodies)
        {
            if(item.IsInGroup("bodies"))
            {
                ((RigidBody)item).ApplyCentralImpulse(-this.GlobalTransform.basis.z * push);
            }

        }

    }

    public override void _UnhandledInput(InputEvent @event)
    {
        if(@event is InputEventMouseMotion)
        {
            InputEventMouseMotion mouseEvent = @event as InputEventMouseMotion;
            RotateY(Mathf.Deg2Rad(-mouseEvent.Relative.x * mouse_sensitivity));

            head.RotateX(Mathf.Deg2Rad(-mouseEvent.Relative.y * mouse_sensitivity));

            Vector3 headRot = head.RotationDegrees;
            headRot.x = Mathf.Clamp(headRot.x, -90, 90);
            head.RotationDegrees = headRot;
        }

    }
}

