using Godot;
using System;

public class ItemsManager : Spatial
{

	[Export]
	private PackedScene[] items;

	private Item currentItem;
	private Pickup pickup;

	private int selectedItem = 0;

	public void init(Pickup _pickup)
	{
		pickup = _pickup;
	}

	public override void _Ready()
	{
		EquipItem(selectedItem);
	}

	public override void _UnhandledInput(InputEvent @event)
	{
		if (@event is InputEventMouseButton)
		{
        	InputEventMouseButton emb = (InputEventMouseButton)@event;

			if(emb is InputEventMouseButton)
			{
				if(@event.IsPressed() && pickup.GetGrabbedObject() == null)
				{
					int previousSelectedItem = selectedItem;

					if(emb.ButtonIndex == 4)
					{
						if(selectedItem >= items.Length - 1)
							selectedItem = 0;
						else
							selectedItem++;
					}
					if(emb.ButtonIndex == 5)
					{
						if(selectedItem <= 0)
							selectedItem = items.Length - 1;
						else
							selectedItem--;
					}

					if(previousSelectedItem != selectedItem)
						EquipNext();
					
					//GD.Print(selectedItem);
				}
			}
		}
	}

    public override void _PhysicsProcess(float delta)
    {
        if(pickup.GetGrabbedObject() != null)
		{
			selectedItem = 0;
			EquipNext();
		}
		
    }

	private void EquipNext()
	{
		UnEquipItem();
		EquipItem(selectedItem);
	}

	private void EquipItem(int index)
	{
		if(items != null)
		{
			for (int i = 0; i < items.Length; i++)
			{
				if(i == index)
				{
					SpawnItem(index);
				}
			}
		}else
			GD.Print("ItemsManager weapons array has no items");
	}

	public void UnEquipItem()
	{
		foreach (Spatial child in this.GetChildren())
		{
			child.QueueFree();
			currentItem = null;
		}
	}

	private void SpawnItem(int index)
	{
		Item item_instance = (Item)items[index].Instance();
		AddChild(item_instance);
		item_instance.GlobalTransform = this.GlobalTransform;
		currentItem = item_instance;
	}

	public Item GetCurrentItem()
	{
		return currentItem;
	}

}
