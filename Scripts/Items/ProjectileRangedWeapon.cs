using Godot;
using System;

public class ProjectileRangedWeapon : Weapon
{
    [Export]
    public PackedScene Bullet;

    // Weapon Points
    Position3D FirePoint;
    Position3D FireDirection;

    // Weapon Data
    Timer attack_cooldown;
    AnimationPlayer animation_player;

    [Export]
    int max_ammo = 10;
    int current_ammo;

    public override void _Ready()
    {
        FirePoint = GetNode<Position3D>("FirePoint");
        FireDirection = GetNode<Position3D>("FireDirection");
        attack_cooldown = GetNode<Timer>("AttackTimer");
        animation_player = GetNode<AnimationPlayer>("AnimationPlayer");

        current_ammo = max_ammo;
    }

    public override void Use()
    {
        if(attack_cooldown.IsStopped() && Bullet != null && current_ammo != 0)
        {

            Bullet bullet_instance = (Bullet)Bullet.Instance();
            Vector3 direction = (FireDirection.GlobalTransform.origin - FirePoint.GlobalTransform.origin).Normalized();

            var global_signal = (GlobalSignals)GetNode("/root/GlobalSignals");
            global_signal.EmitSignal("bullet_fired", bullet_instance, FirePoint, direction);

            animation_player.Play("shot");

            attack_cooldown.Start();

            current_ammo -= 1;
            if(current_ammo <= 0)
            {
                current_ammo = 0;
                //EmitSignal(nameof(weapon_out_of_ammo));
            }
        }
    }
}
